const merge = require('webpack-merge')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const common = require('./webpack.common.js')
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = merge(common, {
    mode: 'production',
    devtool: 'source-map',
    module: {
        rules: [
            { test: /\.ts?$/, exclude: [/(node_modules|bower_components)/], loader: 'ts-loader' },
            { test: /\.js$/, exclude: /(node_modules|bower_components)/, use: [{ loader: 'babel-loader', options: { presets: ['es2017', 'stage-0'] } }] },
            { test: /\.json$/, loader: 'json-loader' },
            { test: /\.scss$/, loader: 'style-loader!css-loader!sass-loader' },
            {
                test: /\.css$/,
                use: [MiniCssExtractPlugin.loader, "css-loader"]
            }

        ]
    },

    plugins: [new ExtractTextPlugin('style.css')]

})