import * as d3 from 'd3';

const protoChart = {
    width: 690,
    height: 480,
    margin: {
        left: 65,
        right: 48,
        top: 33,
        bottom: 63,
    }
};

const radiusVal = {
    min_radius: 10,
    max_radius: 70,
    min_value: 10000,
    max_value: 250000,
    beta: 0.4
};

/* Function to Lighten or Darken stroke colour for Circles */

const LightenDarkenColor = (col, amt) => {
    let usePound = false;
    if (col[0] == "#") {
        col = col.slice(1);
        usePound = true;
    }
    const num = parseInt(col, 16);
    let r = (num >> 16) + amt;
    if (r > 255) r = 255;
    else if (r < 0) r = 18;
    let b = ((num >> 8) & 0x00FF) + amt;
    if (b > 255) b = 255;
    else if (b < 0) b = 18;
    let g = (num & 0x0000FF) + amt;
    if (g > 255) g = 255;
    else if (g < 0) g = 18;
    return (usePound ? "#" : "") + (g | (b << 8) | (r << 16)).toString(16);
}

const map_bubble_size = (v, min_r, max_r, min_v, max_v, b) => {
    let i = v / min_v - 1;
    let n = max_v / min_v - 1;
    //  console.log("i=" + i + " n=" + n);
    return Math.pow((((n - i) / n) * (Math.pow(min_r, b)) + ((i / n) * (Math.pow(max_r, b)))), (1 / b));
}

const chartFactory = (opts, proto = protoChart) => {
    const chart = Object.assign({}, proto, opts);
    chart.svg = d3.select('body div#chart-holder')
        .append('svg')
        .attr('id', chart.id || 'chart')
        .attr('width', chart.width)
        .attr('height', chart.height)
    chart.container = chart.svg.append('g')
        .attr('id', 'container')
        .attr('width', chart.width - chart.margin.left - chart.margin.right)
        .attr('height', chart.height - chart.margin.top - chart.margin.bottom)
        .attr('transform', `translate(${chart.margin.left}, ${chart.margin.top})`)
        .append("g")
        .attr('class', 'mainGrid');
    return chart;
}

export { radiusVal, chartFactory, LightenDarkenColor, map_bubble_size };