import * as d3 from 'd3';
import { radiusVal, chartFactory, LightenDarkenColor, map_bubble_size } from '../common/index';

const remove_duplicates_es6 = (arr) => {
    let s = new Set(arr);
    let it = s.values();
    return Array.from(it);
}

/* Bubble opacity change functionality */
const bubbleOpacity = () => {
    const plotHolder = document.querySelector("g.plot-holder");
    const children = plotHolder.childNodes;
    const elements = [];
    for (let i = 0; i < plotHolder.childNodes.length; i++) {
        const child = plotHolder.childNodes[i];
        if (child.nodeType == 1) {
            elements.push(child.className.animVal)
        }
    }
    const finalElements = remove_duplicates_es6(elements);
    const parentDiv = document.querySelector("g.plot-holder");
    let totalClasses;
    for (let j = 0; j < finalElements.length; j++) {
        totalClasses = document.querySelectorAll(`g.plot-holder circle.${finalElements[j]}`);
        if (totalClasses.length > 1) {
            for (let k = 0; k < totalClasses.length; k++) {
                if (k === 0) {
                    totalClasses[k].style.fillOpacity = 1;
                    totalClasses[k].style.strokeOpacity = 1;
                    // } else if (k === totalClasses.length - 1) {
                    //     totalClasses[k].style.fillOpacity = 0.2;
                    //     totalClasses[k].style.strokeOpacity = 0.2;
                } else {
                    totalClasses[k].style.fillOpacity = ((k / totalClasses.length * 0.5));
                    totalClasses[k].style.strokeOpacity = ((k / totalClasses.length * 0.5));
                }
            }
        } else if (totalClasses.length === 1) {
            for (let k = 0; k < totalClasses.length; k++) {
                if (k === 0) {
                    if (totalClasses[k].style.removeProperty) {
                        totalClasses[k].style.removeProperty('fill-opacity');
                        totalClasses[k].style.removeProperty('stroke-opacity');
                    } else {
                        totalClasses[k].style.removeAttribute('fill-opacity');
                        totalClasses[k].style.removeAttribute('stroke-opacity');
                    }
                }
            }
        }
    }
}

const canvasNew = d3.select("#arcdesign #arcSvg").append("svg").attr("width", 155).attr("height", 105);
let groupNew = canvasNew.append("g").attr("transform", "translate(0 -20)");
const p = Math.PI;
const arc3 = d3.arc()
    .innerRadius(radiusVal.min_radius * 6.875)
    .outerRadius(0)
    .startAngle(0)
    .endAngle(p);
groupNew.append('path')
    .attr('transform', 'translate(71,100)rotate(-90)')
    .attr('d', arc3);
const arc2 = d3.arc()
    .innerRadius(radiusVal.min_radius * 5.5)
    .outerRadius(0)
    .startAngle(0)
    .endAngle(p);
groupNew.append('path')
    .attr('transform', 'translate(57,100)rotate(-90)')
    .attr('d', arc2);
const arc1 = d3.arc()
    .innerRadius(radiusVal.min_radius * 2.75)
    .outerRadius(0)
    .startAngle(0)
    .endAngle(p);
groupNew.append('path')
    .attr('transform', 'translate(30,100)rotate(-90)')
    .attr('d', arc1);
const arc = d3.arc()
    .innerRadius(radiusVal.min_radius)
    .outerRadius(0)
    .startAngle(0)
    .endAngle(p);
groupNew.append('path')
    .attr('transform', 'translate(13,100)rotate(-90)')
    .attr('d', arc);

let arcTextLabels = groupNew.append("g").attr('transform', 'translate(-8,115)');
arcTextLabels.append('text').text('10k').attr('transform', 'translate(25,0)');
arcTextLabels.append('text').text('100k').attr('transform', 'translate(55,0)');
arcTextLabels.append('text').text('200k').attr('transform', 'translate(100,0)');
arcTextLabels.append('text').text('250k').attr('transform', 'translate(135,0)');

let tooltiptop = d3.select("body .tooltip").append("div").attr("class", "tooltiptop");
const yearLabel = d3.select("body #anatomy-holder").append("div")
    .attr("class", "year-label");

let plotPoints;
let cancerTypeInfo;
let cancerTypeInfoLabel;
let survivalRateInfo;
let estimatedDeathsInfo;
let incidenceInfo;
let selectAnatomy;
const selectedYear = '2005';
const chart = chartFactory();
const chartSvg = d3.select("svg#chart");
const dateSlider = d3.select('body #slider-holder')
    .append('svg')
    .attr('width', 700)
    .attr('height', 70)
    .append('g')
    .attr('transform', 'translate(35, 20)');

const anatomyFigure = d3.selectAll('#anatomy-holder #anatomysvg svg > g')
anatomyFigure.attr("fill-opacity", 0);

let filePath;
let maleCounter = 0;
let femaleCounter = 0;
let currentValue;
let tracemode;
let moving;
let playFinished = false;
const mainGrid = d3.select("g.mainGrid");
/* Main init function */
const init = (counterVal) => {
    let timer;
    let targetValue;
    const playButton = d3.select("#play-button");
    const stepCounter = 48.2;
    const range = [0, 625];
    const traceBtn = document.querySelector('.round label');
    clearInterval(timer);
    currentValue = 0;
    timer = 0;
    d3.select("#chart-holder svg#chart g#container g.mainGrid").selectAll("*").remove();
    d3.select("#slider-holder svg g").selectAll("*").remove();
    d3.csv(filePath).then((data, error) => {
        data.forEach(d => {
            d.survival = +d.survival;
            d.death = +d.death;
            d.incidence = +d.incidence;
            d.colour = d.colour;
            d.cancer = d.type;
            d.cancerLabel = d.label;
            d.year = d.year;
        });
        const x = d3.scaleLinear().domain([0, 110]).range([0, chart.width - chart.margin.right - chart.margin.left]);
        const xAxis = d3.axisBottom().scale(x).tickPadding([10]);
        const y = d3.scaleLinear()
            .domain([d3.max(data, d => d.death) + 10000, -5000])
            .range([0, chart.height - chart.margin.top - chart.margin.bottom]);
        const yAxis = d3.axisLeft().scale(y).tickPadding([10]).tickFormat(d => {
            return d3.format(".1s")(d)
        });
        const dateRangeX = d3.scaleLinear()
            .domain([d3.min(data, d => d.year), d3.max(data, d => d.year)])
            .range([0, 625])
            .clamp(true);

        // array useful for step sliders
        let rangeValues = d3.range(range[0], range[1], stepCounter || 1).concat(range[1]);

        //Create a line grid across the width
        chart.container.append('g')
            .attr('class', 'grid')
            .call(d3.axisLeft().scale(y).tickSize(-chart.width + chart.margin.right + chart.margin.right));

        //Add bottom x axis
        chart.container.append('g')
            .attr('transform', 'translate(0, 380)')
            .attr('class', 'xgrid')
            .call(xAxis);

        //Add bottom y axis
        chart.container.append('g')
            .attr('transform', `translate(0, -15)`)
            .attr('class', 'ygrid')
            .call(yAxis)
            .selectAll("text")
            .style("text-anchor", "end")
            .attr("dx", ".7em")
            .attr("dy", "-.55em")
            .attr("transform", "rotate(-90)");

        //Create a line grid across the height
        chart.container.append('g')
            .attr('class', 'grid')
            .attr('transform', `translate(0, 385)`)
            .call(d3.axisBottom().scale(x).tickSize(-380).tickFormat(''));

        //X axis Label
        chart.container.append('text')
            .attr('class', 'x-label')
            .attr('text-anchor', 'middle')
            .attr('transform', `translate(${(chart.width - chart.margin.right) / 2}, ${430})`)
            .text('5-Year Survival Rates (%)');

        //Y axis Label
        chart.container
            .append('text')
            .attr('class', 'y-label')
            .attr('text-anchor', 'middle')
            .attr('dy', '.75em')
            .attr('transform', `rotate(-90) translate(${-((chart.height - chart.margin.bottom - chart.margin.top) / 2)} , ${-(chart.margin.left - 19)})`)
            .text('Estimated Deaths');

        dateSlider.append('line')
            .attr('class', 'track')
            .attr('x1', dateRangeX.range()[0])
            .attr('x2', dateRangeX.range()[1])
            .select(function() { return this.parentNode.appendChild(this.cloneNode(true)); })
            .attr('class', 'track-inset')
            .select(function() { return this.parentNode.appendChild(this.cloneNode(true)); })
            .attr('class', 'track-overlay')
            .call(d3.drag()
                .on('start.interrupt', () => dateSlider.interrupt())
                .on('start drag', () => {
                    renderStats(data, Math.round(dateRangeX.invert(d3.event.x)).toString());
                    dateUpdate(dateRangeX.invert(d3.event.x));
                    yearLabel.html(Math.round(dateRangeX.invert(d3.event.x)).toString());
                    bubbleOpacity();
                })
            ); //update the x location of the slider handle

        targetValue = dateRangeX.range()[1] + 5;

        const dateUpdate = (value) => {
            let x = dateRangeX(value),
                index = null,
                midPoint, cx, xVal;
            if (stepCounter) {
                // if step has a value, compute the midpoint based on range values and reposition the slider based on the mouse position
                for (let i = 0; i < rangeValues.length - 1; i++) {
                    if (x >= rangeValues[i] && x <= rangeValues[i + 1]) {
                        index = i;
                        break;
                    }
                }
                midPoint = (rangeValues[index] + rangeValues[index + 1]) / 2;
                if (x < midPoint) {
                    cx = dateRangeX(rangeValues[index]);
                    xVal = rangeValues[index];
                } else {
                    cx = dateRangeX(rangeValues[index + 1]);
                    xVal = rangeValues[index + 1];
                }
            } else {
                // if step is null or 0, return the drag value as is
                cx = x;
                xVal = x.toFixed(2);
            }
            sliderHandle.attr('cx', xVal);
            sliderHandleText.attr('x', xVal);
        }
        /*
                const dateUpdate = (value) => {
                    sliderHandle.attr('cx', dateRangeX(value));
                    sliderHandleText.attr('x', dateRangeX(value));
                }
        */
        const step = () => {
            dateUpdate(dateRangeX.invert(currentValue));
            renderStats(data, Math.round(dateRangeX.invert(currentValue)).toString());
            yearLabel.html(Math.round(dateRangeX.invert(currentValue)).toString());
            currentValue = currentValue + (targetValue / 151);
            if (currentValue > targetValue) {
                moving = false;
                currentValue = 0;
                clearInterval(timer);
                timer = 0;
                playButton.text("Play");
                playFinished = true;
            }
        }

        /* Play button functionality */
        playButton
            .on("click", () => {
                const button = d3.select(this);
                clearInterval(timer);
                //timer = 0;
                moving = true;
                timer = setInterval(step, 100);
                //                if (button.text() == "Pause") {
                //   moving = false;
                //   clearInterval(timer);
                //   timer = 0;
                //   button.text("Play");
                //              } else {
                //               moving = true;
                //                timer = setInterval(step, 100);
                //                button.text("Pause");
                //}
            })

        //draw and render the date slider
        dateSlider.insert('g', '.track-overlay')
            .attr('class', 'ticks')
            .attr('transform', 'translate(0,' + 35 + ')')
            .selectAll('text')
            .data(dateRangeX.ticks(14))
            .enter().append('text')
            .attr('x', dateRangeX)
            .attr('text-anchor', 'middle')
            .text(d => d);

        const sliderHandle = dateSlider.insert('circle', '.track-overlay')
            .attr('class', 'handle')
            .attr('r', 15);

        const sliderHandleText = dateSlider.insert('g', '.track-overlay')
            .attr('transform', 'translate(-6,' + 5 + ')')
            .append('text')
            .attr('class', 'handleText')
            .text('III');

        //draw and render the date slider date labels
        yearLabel.html(selectedYear);
        const plotPointsHolder = chart.container.append('g')
            .attr('class', 'plot-holder');
        //Main function that draws and plots the circle on the chart, gets called on init and on dateslider update
        const renderStats = (stats, year) => {
            //if (tracemode) {
            //     plotPoints = plotPointsHolder.selectAll('circle').data(stats.filter((d) => d.year <= year));
            // } else {
            //     plotPoints = plotPointsHolder.selectAll('circle').data(stats.filter((d) => d.year === year));
            // }
            plotPoints = (tracemode) ? plotPointsHolder.selectAll('circle').data(stats.filter((d) => d.year <= year)) : plotPointsHolder.selectAll('circle').data(stats.filter((d) => d.year === year));

            plotPoints.exit()
                .remove();
            plotPoints.enter()
                .append('circle')
                .style('stroke', d => LightenDarkenColor(`${d.colour}`, -50))
                .style('shape-rendering', 'auto')
                .style('stroke-width', '1.4')
                .style('fill', d => `${d.colour}`)
                .attr('class', d => `${d.type}`)
                .on('mouseover', function() {
                    renderInfoMarker(this, "Circle", this.style.fill);
                })
                .on('mouseout', function() {
                    d3.select(".tooltiptop").style("display", "none");
                    removeRenderMarker(this, "Circle");
                })
                .merge(plotPoints)
                .style('stroke', d => LightenDarkenColor(`${d.colour}`, -50))
                .style('shape-rendering', 'auto')
                .style('stroke-width', '1.4')
                .style('fill', d => `${d.colour}`)
                .attr('class', d => `${d.type}`)
                .attr('cx', d => x(d.survival))
                .attr('cy', d => y(d.death))
                .attr('r', d => map_bubble_size(d.incidence, radiusVal.min_radius, radiusVal.max_radius, radiusVal.min_value, radiusVal.max_value, radiusVal.beta))
                .transition()
                .duration(150)
                .ease(d3.easeLinear)
                .style('stroke', d => LightenDarkenColor(`${d.colour}`, -130))
                .attr('cx', d => x(d.survival))
                .attr('cy', d => y(d.death))
                .attr('r', d => map_bubble_size(d.incidence, radiusVal.min_radius, radiusVal.max_radius, radiusVal.min_value, radiusVal.max_value, radiusVal.beta));
            bubbleOpacity();
        }
        // Opacity and the text is back to default values when mouse over on Anatomy SVG or circles.
        //renderStats(data, selectedYear)

        /* Trace events functionality */
        traceBtn.onclick = function(el) {
            if (!tracemode) {
                tracemode = true;
                el.checked = true;
                if (!moving) {
                    renderStats(data, document.querySelector(".year-label").innerText);
                }
            } else {
                tracemode = false;
                el.checked = false;
                if (!moving) {
                    renderStats(data, document.querySelector(".year-label").innerText);
                }
            };
            bubbleOpacity();
        }
        if (counterVal === 1) {
            document.querySelector("#play-button").click();
        } else {
            currentValue = 0;
            renderStats(data, Math.round(dateRangeX.invert(currentValue)).toString());
        }
    });

    /* Click function to interrupt the animation */
    chartSvg.on("click", () => {
        if (moving) {
            moving = false;
            clearInterval(timer);
            timer = 0;
        }
    });
}

/* Male button click functionality */
d3.select(".btnMale").on("click", () => {
    if (selectAnatomy !== "malesvg") {
        selectAnatomy = "malesvg";
        d3.select("#femalesvg").style("display", "none").style("z-index", 0);
        d3.select("#malesvg").style("display", "block").style("z-index", 1);
        filePath = "/data/Male_Data_v3.csv";
        maleCounter = maleCounter + 1;
        const anatomyMaleOutline = d3.selectAll('#anatomy-holder #anatomysvg svg > g#male');
        anatomyMaleOutline.attr("fill-opacity", 1);
        tracemode = document.querySelector("#checkbox").checked ? true : false;
        //if (document.querySelector("#checkbox").checked === true) {
        //     tracemode = true;
        // } else {
        //     tracemode = false;
        // }
        init(maleCounter);
    }
})

/* Female button click functionality */
d3.select(".btnFemale").on("click", () => {
    if (selectAnatomy !== "femalesvg") {
        selectAnatomy = "femalesvg";
        d3.select("#malesvg").style("display", "none").style("z-index", 0);
        d3.select("#femalesvg").style("display", "block").style("z-index", 1);
        filePath = "/data/Female_Data_v2.csv";
        femaleCounter = femaleCounter + 1;
        const anatomyFemaleOutline = d3.selectAll('#anatomy-holder #anatomysvg svg > g#female');
        anatomyFemaleOutline.attr("fill-opacity", 1);

        tracemode = document.querySelector("#checkbox").checked ? true : false;
        //if (document.querySelector("#checkbox").checked === true) {
        //    tracemode = true;
        //} else {
        //    tracemode = false;
        // }
        init(femaleCounter);
    }
})

/* Mouse out functionality on circle*/
const removeRenderMarker = (hitTarget, element) => {
    d3.select("#arcSvg svg g path.finalArc").remove();
    d3.select(hitTarget.parentNode).select('circle.info').remove()
    anatomyFigure.attr("fill-opacity", 0);
    const anatomyFigureHighlight = d3.select(`#anatomy-holder #anatomysvg #${selectAnatomy} svg > g`);
    //anatomyFigureHighlight.attr("stroke-width", 0).attr("fill-opacity", 0.3);
    anatomyFigureHighlight.attr("stroke-width", 0).attr("fill-opacity", 0);
}

/* Mouse over functionality on circle*/
const renderInfoMarker = (hitTarget, element, color) => {
    if (!moving) {
        let radVal = hitTarget.getAttribute("r");
        let xLoc;
        if (radVal < 10) {
            radVal = radiusVal.min_radius;
            xLoc = radVal * 1.3;
        } else {
            xLoc = parseInt(radVal) + 2;
        }
        const newArc = d3.select("#arcdesign #arcSvg svg g")
        const p = Math.PI;
        const finalArc = d3.arc()
            .innerRadius(radVal)
            .outerRadius(0)
            .startAngle(0)
            .endAngle(p);
        newArc.insert('path', ':first-child')
            .attr("class", "finalArc")
            .style("fill", color)
            .attr('transform', `translate(${(xLoc)}, ${100}) rotate(-90)`)
            .attr('d', finalArc);
        const target = d3.select(hitTarget);
        const targetParent = d3.select(hitTarget.parentNode);
        cancerTypeInfo = target.datum().cancer;
        cancerTypeInfoLabel = target.datum().cancerLabel;
        incidenceInfo = target.datum().incidence;
        survivalRateInfo = target.datum().survival + '%';
        estimatedDeathsInfo = target.datum().death;

        targetParent.insert('circle', ':first-child')
            .attr('class', 'info')
            .attr('cx', target.attr('cx'))
            .attr('cy', target.attr('cy'))
            .style('stroke', color)
            .style('stroke-width', '3')
            .style('shape-rendering', 'auto')
            .attr('fill-opacity', '0')
            .attr('r', '1')
            .transition()
            .duration(100)
            .attr('r', `${(target.attr('r')) * 1.25}`);

        tooltiptop.transition()
            .duration(300)
            .style("display", "block");

        const gridWidth = 594;
        const gridHeight = 384;
        const tooltipWidth = 224;
        const tooltipHeight = 113;
        let finalX;
        let finalY;

        if (target.attr('cx') > (gridWidth / 2)) {
            finalX = parseInt(target.attr('cx')) - 10;
        } else {
            finalX = parseInt(target.attr('cx')) + 100;
        }
        if (target.attr('cy') > (gridHeight / 2)) {
            finalY = parseInt(target.attr('cy')) + 175;
        } else {
            finalY = parseInt(target.attr('cy')) + 360;
        }
        tooltiptop.html(`<p>${cancerTypeInfoLabel}</p><p><span>Incidence Rate:</span> <span>${incidenceInfo}</span></p><p><span>Estimated Deaths:</span> <span>${estimatedDeathsInfo}</span></p><p><span>5-year Survival Rate (%):</span> <span>${survivalRateInfo}</span></p>`).style("left", finalX + "px").style("top", finalY + "px");
        //tooltiptop.html(`<p>${cancerTypeInfo}</p><p><span>Estimated Deaths:</span> <span>${estimatedDeathsInfo}</span></p><p><span>5-year Survival<br/>Rates (%):</span> <span>${survivalRateInfo}</span></p>`);
        if (element == "Circle") {
            anatomyUpdate();
        }
    }
}

const removeTooltip = (e) => {
    if (e.target.tagName !== "circle") {
        d3.select(".tooltiptop").style("display", "none");
    }
};

document.addEventListener('mouseover', removeTooltip, false);
document.addEventListener('mouseenter', removeTooltip, false);
document.addEventListener('mousemove', function(e) {
    if (e.target.tagName === "BODY") {
        d3.select(".tooltiptop").style("display", "none");
    }
}, true);

// Opacity and the text is updated when mouse over on Anatomy SVG.
const anatomyUpdate = () => {
    const anatomyFigureHighlight = d3.select(`#anatomy-holder #anatomysvg #${selectAnatomy} svg > g#${cancerTypeInfo}`);
    //anatomyFigure.attr("fill-opacity", 0.3);
    anatomyFigure.attr("fill-opacity", 0);
    anatomyFigureHighlight
        .attr("fill-opacity", 1)
        .attr("stroke-width", 1);
}
/* Male button clicked by default at beginning */
(() => {
    document.querySelector(".btnMale").click();
})();