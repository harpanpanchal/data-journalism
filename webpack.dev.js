const merge = require('webpack-merge')
const common = require('./webpack.common.js')


module.exports = merge(common, {
    mode: 'development',
    devtool: 'inline-source-map',
    module: {
        rules: [
            { test: /\.ts?$/, exclude: [/(node_modules|bower_components)/], loader: 'ts-loader' },
            { test: /\.js$/, exclude: /(node_modules|bower_components)/, use: [{ loader: 'babel-loader', options: { presets: ['es2017', 'stage-0'] } }] },
            { test: /\.json$/, loader: 'json-loader' },
            { test: /\.scss$/, loader: 'style-loader!css-loader!sass-loader' }

        ]
    }
})