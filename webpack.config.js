const path = require('path');
const mode = process.env.NODE_ENV || "development";

const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
    mode: "development",
    entry: "./lib/main.js",
    output: {
        path: path.resolve(__dirname, 'build'),
        publicPath: '/build/',
        filename: 'bundle.js'
    },
    optimization: {
        minimizer: [new UglifyJsPlugin()]
    },
    module: {
        rules: [
            { test: /\.ts?$/, exclude: [/(node_modules|bower_components)/], loader: 'ts-loader' },
            { test: /\.js$/, exclude: /(node_modules|bower_components)/, use: [{ loader: 'babel-loader', options: { presets: ['es2017', 'stage-0'] } }] },
            { test: /\.json$/, loader: 'json-loader' },
            { test: /\.scss$/, loader: 'style-loader!css-loader!sass-loader' },
            {
                test: /\.css$/,
                use: [MiniCssExtractPlugin.loader, "css-loader"]
            }

        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: "[name].css",
            chunkFilename: "[id].css"
        })
    ]
};